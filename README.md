# 1. Curso de Microservices com Spring Cloud: Registry, Config Server e Distributed Tracing

Nesse curso aprendi como funciona a decomposição do monólito, implementei o Service Discovery com Eureka e centralizei as configurações com Spring Config Server. Usei o Spring Feign que usa o Ribbon para Client-side Load Balancer, aumentando a disponibilidade do serviço, e o Spring Sleuth para gerar a correlation-id para monitorar e depurar os logs das requisições no Papertrail (Agregador de logs).

# 2. Curso de Microservices com Spring Cloud: Circuit Breaker, Hystrix e API Gateway
Na continuação do curso de Microservices usei um API Gateway com Spring Zuul para criar um único ponto de acesso à aplicação e implementei um servidor de autenticação 
e autorização com token, integrando os microserviços à ele.
Usei o Circuit Breaker com Hystrix, limitando o tempo de processamento das requisições para 1 segundo, em conjunto com o Fallback para tratar a interrupção da Thread efetuada pelo Circuit Breaker e tratei 
os erros em um sistema distribuído usando o Bulkhead Pattern com o Hystrix.
