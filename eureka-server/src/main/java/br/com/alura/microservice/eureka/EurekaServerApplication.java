package br.com.alura.microservice.eureka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class EurekaServerApplication {
	
	/* Implementação do service registry através do Eureka Server. 
	   Service registry é um servidor central, onde todos os microsserviços ficam cadastrados (nome e IP/porta) */

	public static void main(String[] args) {
		SpringApplication.run(EurekaServerApplication.class, args);
	}

}
