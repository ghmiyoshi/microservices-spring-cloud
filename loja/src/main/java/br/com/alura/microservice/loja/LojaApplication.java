package br.com.alura.microservice.loja;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;
import org.springframework.web.client.RestTemplate;

import feign.RequestInterceptor;
import feign.RequestTemplate;

@SpringBootApplication
@EnableFeignClients // Habilito o feign client
@EnableCircuitBreaker
@EnableResourceServer
public class LojaApplication {
	
	@Bean // Crio esse interceptor para passar o token para as requisições dos outros microserviços feitas pelo Feign
	public RequestInterceptor getInterceptorDeAutenticacao() {
		return new RequestInterceptor() {

			@Override
			public void apply(RequestTemplate template) {
				Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

				if (authentication == null) {
					return;
				}

				OAuth2AuthenticationDetails details = (OAuth2AuthenticationDetails) authentication.getDetails();

				template.header("Authorization", "Bearer" + details.getTokenValue());
			}
		};
	}
	
	@Bean // Digo que será um bean gerenciado pelo Spring e assim consigo injetar uma instância do RestTemplate
	@LoadBalanced /* Essa anotação resolve o acesso a http://fornecedor/info/ no Eureka e adiciona a funcionalidade de load balanced, ou seja, 
	distribui as requisições vindas dos usuários para as várias instâncias disponíveis. */
	
	// Pego as informações do Eureka e troco pelo IP e Porta do serviço fornecedor que estiver rodando 	
	public RestTemplate getRestTemplate() {
		return new RestTemplate();
	}
	
	/* Service discovery - Mecanismo de descoberta do IP do microsserviço pelo nome
	 
	   Client Side Load Balancer - A Aplicação cliente conhece as instâncias disponíveis e, usando algum algoritmo de rotação do Ribbon, acessa uma instância diferente a cada requisição do usuário 

	   Circuit Breaker - Implementado com Hystrix e tem como funcionalidade principal a análise das requisições anteriores, para decidir se deve parar de repassar as requisições vindas do cliente para um microsserviço com 
	   problemas de performance. Enquanto o circuito está fechado, o Hystrix continua tentando a cada 5 segundos, com o objetivo de verificar se o servidor voltou a funcionar normalmente
	   
	   Fallback - Funcionalidade de tratamento para uma execução que foi "cortada" pelo Circuit Breaker 
	   
	   Bulkhead - Implementado com o Hystrix e pode agrupar e alocar grupos de threads para processamentos diferentes. Dessa forma, uma das chamadas de um microsserviço, que sofre lentidão por causa de uma integração com 
	   problema de performance, não indisponibiliza todas as outras chamadas do mesmo microsserviço */
	
	public static void main(String[] args) {
		SpringApplication.run(LojaApplication.class, args);
	}

}
