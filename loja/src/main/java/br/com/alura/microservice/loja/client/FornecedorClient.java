package br.com.alura.microservice.loja.client;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import br.com.alura.microservice.loja.dto.InfoFornecedorDTO;
import br.com.alura.microservice.loja.dto.InfoPedidoDTO;
import br.com.alura.microservice.loja.dto.ItemDaCompraDTO;

@FeignClient("fornecedor") // id, nome do microserviço que eu desejo acessar
public interface FornecedorClient {
	
	@GetMapping("/info/{estado}") // Path do serviço de fornecedor
	InfoFornecedorDTO getInfoPorEstado(@PathVariable String estado); // É o mesmo nome do método que executa um GET na API de fornecedor

	@PostMapping("/pedido")
	InfoPedidoDTO realizaPedido(List<ItemDaCompraDTO> itens);

	
	
}
