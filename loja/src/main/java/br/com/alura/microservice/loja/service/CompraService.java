package br.com.alura.microservice.loja.service;

import java.time.LocalDate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.stereotype.Service;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

import br.com.alura.microservice.loja.client.FornecedorClient;
import br.com.alura.microservice.loja.client.TransportadorClient;
import br.com.alura.microservice.loja.dto.CompraDTO;
import br.com.alura.microservice.loja.dto.InfoEntregaDTO;
import br.com.alura.microservice.loja.dto.InfoFornecedorDTO;
import br.com.alura.microservice.loja.dto.InfoPedidoDTO;
import br.com.alura.microservice.loja.dto.VoucherDTO;
import br.com.alura.microservice.loja.model.Compra;
import br.com.alura.microservice.loja.model.CompraState;
import br.com.alura.microservice.loja.repository.CompraRepository;

@Service
public class CompraService {
	
	private static final Logger LOG = LoggerFactory.getLogger(CompraService.class);

	@Autowired
	private FornecedorClient fornecedorClient;
	
	@Autowired
	private TransportadorClient transportadorClient;
	
	@Autowired
	private CompraRepository compraRepository;
	
	@Autowired
	private DiscoveryClient eurekaClient;
	
	@HystrixCommand(fallbackMethod = "realizaCompraFallback", threadPoolKey = "realizaCompraThreadPool") // Indico o método que vai responder para o usuário depois que o circuit breaker forçar uma falha, ou seja, quando a requisição demorar mais de um segundo 
	public Compra realizaCompra(CompraDTO compra) {	
		
		// Gero uma compra
		Compra compraSalva = new Compra();
		
		compraSalva.setState(CompraState.RECEBIDO);
		compraSalva.setEnderecoDeDestino(compra.getEndereco().toString());
		compraRepository.save(compraSalva);
		compra.setCompraId(compraSalva.getId());
		
		LOG.info("Buscando informações do fornecedor de {}",compra.getEndereco().getEstado());	
		InfoFornecedorDTO infoPorEstado = fornecedorClient.getInfoPorEstado(compra.getEndereco().getEstado());  
		
		// Faço um pedido no microserviço de fornecedor
		LOG.info("Realizando pedido...");
		InfoPedidoDTO pedido = fornecedorClient.realizaPedido(compra.getItens());
		
		compraSalva.setState(CompraState.PEDIDO_REALIZADO);
		compraSalva.setPedidoId(pedido.getId());
		compraSalva.setTempoDePreparo(pedido.getTempoDePreparo());
		compraRepository.save(compraSalva);
		
		// Exemplo para lançar uma exception e direcionar para o método fallback
		// if(1==1)throw new RuntimeException(); 
		
		// Mostro no console as instâcias registradas no Eureka do microserviço de fornecedor
		eurekaClient.getInstances("fornecedor").stream().forEach(fornecedor -> System.out.println("URI: " + fornecedor.getUri()));
		
		System.out.println("Endereço do fornecedor: " + infoPorEstado.getEndereco());
		System.out.println("Informações do pedido: " + pedido);
		
		// Defino os mesmos atributos da EntregaDTO do microserviço de transportador  
		InfoEntregaDTO entregaDto = infoEntrega(compra, infoPorEstado, pedido);
		
		// Passo as informações de entrega para criar um voucher no microserviço de transportador
		VoucherDTO voucher = transportadorClient.reservaEntrega(entregaDto);
		
		compraSalva.setState(CompraState.RESERVA_ENTREGA_REALIZADA);
		compraSalva.setDataParaEntrega(voucher.getPrevisaoParaEntrega());
		compraSalva.setVoucher(voucher.getNumero());
		compraRepository.save(compraSalva);
		
		return compraSalva;		
	}

	private InfoEntregaDTO infoEntrega(CompraDTO compra, InfoFornecedorDTO infoPorEstado, InfoPedidoDTO pedido) {
		InfoEntregaDTO entregaDto = new InfoEntregaDTO();
		
		entregaDto.setPedidoId(pedido.getId());
		entregaDto.setDataParaEntrega(LocalDate.now().plusDays(pedido.getTempoDePreparo()));
		entregaDto.setEnderecoOrigem(infoPorEstado.getEndereco());
		entregaDto.setEnderecoDestino(compra.getEndereco().toString());
		
		return entregaDto;
	}	

	/* threadPoolKey - uso para agrupar e alocar grupos de threads para processamentos diferentes. Dessa forma, 
	 uma das chamadas de um microsserviço, que sofre lentidão por causa de uma integração com problema de performance, 
	 não indisponibiliza todas as outras chamadas do mesmo microsserviço */
	@HystrixCommand(threadPoolKey = "buscaCompraThreadPool") 
	public Compra buscaCompra(Long id) {
		return compraRepository.findById(id).orElse(new Compra());
	}
	
	// Crio uma funcionalidade de tratamento para uma execução que foi "cortada" pelo Circuit Breaker
	public Compra realizaCompraFallback(CompraDTO compra) {
		// Se existir um id, retorno para o cliente os dados da compra com status PEDIDO_REALIZADO
		if(compra.getCompraId() != null) {
			return compraRepository.findById(compra.getCompraId()).get();
		}
		
		// Se não, retorno dados de compra null
		Compra compraFallBack = new Compra();
		compraFallBack.setEnderecoDeDestino(compra.getEndereco().toString());
		
		return compraFallBack;
	}
	
}
